var app = angular.module("spr", ["ngSanitize"]).controller("drawController", function ($scope, $rootScope) {

    //canvas height/width
    $scope.canvasHeight = 830;
    $scope.canvasWidth = 1224;

    $scope.globalAlphaBool = true;
    $scope.globalAlpha = 1;
    $scope.showHide = function () {
        if ($scope.globalAlphaBool) {
            $scope.globalAlpha = 1;
        } else {
            $scope.globalAlpha = 0;
        }
        redraw();
    };

    //color list
    $scope.colors = ["#3399FF", "#CD0000"];

    //tool list
    $scope.tools = ["Marker", "Eraser", "Pan"];

    //zoom list
    $scope.zooms = [{
        dir: "zoom-out",
        out: true
    }, {
        dir: "zoom-in",
        out: false
    }];

    $scope.masterDraw = {
        clickX: [],
        clickY: [],
        clickDrag: [],
        clickColor: [],
        clickTool: []
    };

    //current color and changing colors
    $scope.curColorIndex = 0;
    $scope.curColor = $scope.colors[$scope.curColorIndex];
    $scope.changeColor = function (i) {
        $scope.curColorIndex = i;
        $scope.curColor = $scope.colors[i];
        $scope.curToolIndex = 0;
        angular.element(canvas).css("cursor", "auto");
    };

    //current tool and changing tools
    $scope.curToolIndex = 0;
    $scope.changeTool = function (i) {
        $scope.curToolIndex = i;
        if ($scope.curToolIndex === 2) {
            angular.element(canvas).css("cursor", "move");
        } else {
            angular.element(canvas).css("cursor", "auto");
        }
    };

    //pan start values and changing pan
    $scope.panX = 0;
    $scope.panY = 0;

    $scope.resetCanvas = function () {
        $scope.panX = 0;
        $scope.panY = 0;
        $scope.masterZoom = 1;
        redraw();
    };

    //zoom start value and changing zoom
    $scope.masterZoom = 1;
    $scope.changeZoom = function (z) {
        if (z.out) {
            if (($scope.masterZoom / 1.1) < 1) {
                return;
            }
            $scope.masterZoom /= 1.1;
        } else {
            if (($scope.masterZoom * 1.1) > 6) {
                return;
            }
            $scope.masterZoom *= 1.1;
        }
        redraw();
    };

    //clear canvas
    $scope.clear = function () {
        $scope.masterDraw.clickX = [];
        $scope.masterDraw.clickY = [];
        $scope.masterDraw.clickDrag = [];
        $scope.masterDraw.clickColor = [];
        $scope.masterDraw.clickTool = [];
        $scope.curToolIndex = 0;
        angular.element(canvas).css("cursor", "auto");
        clearCanvas();

        //draw background image
        context.drawImage($scope.prodImg, -$scope.panX, -$scope.panY, $scope.canvasWidth, $scope.canvasHeight, 0, 0, $scope.canvasWidth * $scope.masterZoom, $scope.canvasHeight * $scope.masterZoom);
    };

    $scope.download = function (e) {
        $scope.resetCanvas();
        e.target.href = canvas.toDataURL();
    };

    //create canvas and set attributes
    var canvasDiv = document.getElementById("container");
    var canvas = document.createElement("canvas");
    canvasDiv.setAttribute("width", $scope.canvasWidth);
    canvasDiv.setAttribute("height", $scope.canvasHeight);
    canvas.setAttribute("width", $scope.canvasWidth);
    canvas.setAttribute("height", $scope.canvasHeight);
    canvas.setAttribute("id", "canvas");
    canvasDiv.appendChild(canvas);

    //create canvas context
    var context = canvas.getContext("2d");

    //background image
    $rootScope.$watch("selectedProd", function (n) {
        if (n.value) {
            $scope.prodImg = new Image();
            $scope.prodImg.src = "imgs/svg/" + n.value + ".svg";
            $scope.prodImg.onload = function () {
                context.drawImage($scope.prodImg, $scope.panX, $scope.panY, $scope.canvasWidth, $scope.canvasHeight, 0, 0, $scope.canvasWidth * $scope.masterZoom, $scope.canvasHeight * $scope.masterZoom);
            };
        }
    });

    //mousedown event
    $scope.xStart = 0;
    $scope.yStart = 0;
    angular.element(canvas).on("mousedown", function () {
        $scope.paint = true;
        if ($scope.curToolIndex === 2) {
            $scope.xStart = parseInt($scope.mouseX - $scope.panX);
            $scope.yStart = parseInt($scope.mouseY - $scope.panY);
            $scope.$apply();
        } else {
            addClick($scope.mouseXT, $scope.mouseYT, false);
        }

        redraw();
    });

    //mousemove event
    angular.element(canvas).on("mousemove", function (e) {

        //relative mouse position (no pan/zoom)
        $scope.mouseX = parseInt(e.clientX - canvas.getBoundingClientRect().left);
        $scope.mouseY = parseInt(e.clientY - canvas.getBoundingClientRect().top);

        //actual mouse position (pan/zoom accounted for)
        $scope.mouseXT = parseInt(($scope.mouseX - ($scope.panX * $scope.masterZoom)) / $scope.masterZoom);
        $scope.mouseYT = parseInt(($scope.mouseY - ($scope.panY * $scope.masterZoom)) / $scope.masterZoom);

        if ($scope.curToolIndex === 2) {
            if ($scope.paint) {
                $scope.panX = -$scope.xStart + $scope.mouseX;
                $scope.panY = -$scope.yStart + $scope.mouseY;
                redraw();
            }
        } else {
            if ($scope.paint) {
                addClick($scope.mouseXT, $scope.mouseYT, true);
                redraw();
            }
        }

        //apply so scope can see changes
        $scope.$apply();
    });

    //mouseup event
    angular.element(canvas).on("mouseup", function () {
        $scope.paint = false;
        redraw();
    });

    //mouseleave event
    angular.element(canvas).on("mouseleave", function () {
        $scope.paint = false;
    });

    //add x/y coordinates, drag boolean, current tool/color to respective arrays
    function addClick(x, y, dragging) {
        $scope.masterDraw.clickX.push(x);
        $scope.masterDraw.clickY.push(y);
        $scope.masterDraw.clickDrag.push(dragging);
        $scope.masterDraw.clickTool.push($scope.curToolIndex);
        if ($scope.curToolIndex === 1) {
            $scope.masterDraw.clickColor.push("#ffffff");
        } else {
            $scope.masterDraw.clickColor.push($scope.curColor);
        }
    }

    //clear canvas
    function clearCanvas() {
        context.clearRect(0, 0, $scope.canvasWidth, $scope.canvasHeight);
    }

    function redraw() {

        //clear canvas
        clearCanvas();

        context.save();

        context.globalAlpha = 1;

        context.lineWidth = 5;

        //rounded points
        context.lineJoin = "round";

        context.scale($scope.masterZoom, $scope.masterZoom);
        context.translate($scope.panX, $scope.panY);

        //set background to white
        context.fillStyle = "#fff";
        context.fillRect(0, 0, $scope.canvasWidth, $scope.canvasHeight);

        //for each points in array
        for (var i = 0; i < $scope.masterDraw.clickX.length; i++) {

            context.beginPath();
            if ($scope.masterDraw.clickDrag[i] && i) {
                context.moveTo($scope.masterDraw.clickX[i - 1], $scope.masterDraw.clickY[i - 1]);
            } else {
                context.moveTo($scope.masterDraw.clickX[i] - 1, $scope.masterDraw.clickY[i]);
            }
            context.lineTo($scope.masterDraw.clickX[i], $scope.masterDraw.clickY[i]);
            context.closePath();
            if ($scope.masterDraw.clickTool[i] == 1) {
                context.strokeStyle = "white";
            } else {
                context.strokeStyle = $scope.masterDraw.clickColor[i];
            }
            context.stroke();
        }

        context.restore();

        context.globalAlpha = $scope.globalAlpha;

        //draw background image
        context.drawImage($scope.prodImg, -$scope.panX, -$scope.panY, $scope.canvasWidth, $scope.canvasHeight, 0, 0, $scope.canvasWidth * $scope.masterZoom, $scope.canvasHeight * $scope.masterZoom);
    }

}).run(function ($rootScope, $timeout) {

    $rootScope.translationObj = {
        en: {
            tools: ["Marker", "Eraser", "Pan"],
            breadCrumbs: ["select set/system", "show problem area on diagram", "input incident information", "submit report"],
            h1Title: "SET/SYSTEM PERFORMANCE REPORTS",
            h2Title: "Select your set/system",
            h2TitlePostSelect: "SYSTEM PERFORMANCE REPORT",
            welcome: "WELCOME",
            para1: "Terumo BCT is committed to quality. As such, we welcome your comments, suggestions and concerns. The list of performance reports below will help ensure we understand any problems you are experiencing with our disposable tubing sets.",
            para2: "To help us understand the problem you are experiencing and reach a prompt resolution, please follow these simple steps:",
            list: ["Show problem area on the Interactive Diagram", "Fill out the Incident Information Form"],
            para3: "Once your form is submitted, a member of our Customer Support team will address your issues.",
            beginButton: "Begin",
            QorC: "Questions or concerns?",
            tollFree: "toll free",
            phoneNumber: "1.877.339.4BCT (4228)",
            aRR: "All Rights Reserved",
            quickOverview: "Click Here for a Quick Overview",
            startOver: "Start Over",
            rfad: "Review Form and Diagram",
            backToDiagram: "Back To Diagram",
            reviewAndSubmit: "Review And Submit",
            backToEdit: "Back To Edit",
            print: "Print For Your Records",
            submit: "Submit",
            toolList: ["Color", "Tool", "Zoom", "Show Background", "Clear", "Done"],
            toolTipList: {
                color: "Change Color",
                tool: "Change Tool",
                zoom: "Zoom In/Out",
                background: "Toggle Background",
                clear: "Clear Drawings",
                save: "Save and Continue"
            },
            products: [{
                value: "",
                label: "Select Product..."
    }, {
                value: null,
                label: "------------------------------------------------------------------------------------------"
    }, {
                value: null,
                label: "Blood Centers"
    }, {
                value: null,
                label: "------------------------------------------------------------------------------------------"
    }, {
                value: "Trima-PLT-RBC-PL",
                label: htmlDecode("Trima Accel&reg; Platelet/Plasma/RBC Set"),
                form: "trima"
    }, {
                value: "Trima-DRBC",
                label: htmlDecode("Trima Accel&reg; RBC, Plasma Set"),
                form: "trima"
    }, {
                value: "Teruflex",
                label: htmlDecode("Teruflex&reg; Blood Bag System"),
                form: "teruflex"
    }, {
                value: "Imuflex-WB-SP",
                label: htmlDecode("Imuflex&reg; Blood Bags - SP"),
                form: "imuflex"
    }, {
                value: "Imuflex-WB-RP",
                label: htmlDecode("Imuflex&reg; Blood Bags - RP"),
                form: "imuflex"
    }, {
                value: null,
                label: "------------------------------------------------------------------------------------------"
    }, {
                value: null,
                label: "Cell Processing"
    }, {
                value: null,
                label: "------------------------------------------------------------------------------------------"
    }, {
                value: "COBE-2991",
                label: htmlDecode("COBE&reg; 2991 Cell Processor"),
                form: "cobe2991"
    }, {
                value: "Elutra",
                label: htmlDecode("Elutra&reg; Cell Separation System"),
                form: "elutra"
    }, {
                value: null,
                label: "------------------------------------------------------------------------------------------"
    }, {
                value: null,
                label: "Therapeutic Apheresis and Cell Collections"
    }, {
                value: null,
                label: "------------------------------------------------------------------------------------------"
    }, {
                value: "Cobe-Spectra-Collection",
                label: htmlDecode("COBE&reg; Spectra White Blood Cell Set"),
                form: "cobespectra"
    }, {
                value: "Cobe-Spectra-TPE",
                label: htmlDecode("COBE&reg; Spectra TPE/RBC Exchange Set"),
                form: "cobespectra"
    }, {
                value: "Cobe-Spectra-AutoPBSC",
                label: htmlDecode("COBE&reg; Spectra AutoPBSC Set"),
                form: "cobespectra"
    }, {
                value: "OptiaExchange",
                label: htmlDecode("Spectra Optia&reg; Apheresis System Exchange Set"),
                form: "optiaexchange"
    }, {
                value: "OptiaCollection",
                label: htmlDecode("Spectra Optia&reg; Apheresis System Collection Set"),
                form: "optiacollection"
}]
        },
        fr: {
            tools: ["Dessin", "Effacer", "Main"],
            breadCrumbs: [htmlDecode("S&eacute;lectionnez SET/syst&egrave;me"), htmlDecode("montrer zone &agrave; probl&egrave;me sur le diagramme"), htmlDecode("information sur un incident d'entr&eacute;e"), "soumettre le rapport"],
            h1Title: htmlDecode("RAPPORTS DE PERFORMANCE SUR LE KIT/SYST&egrave;ME"),
            h2Title: htmlDecode("S&eacute;LECTIONNER VOTRE KIT/SYST&egrave;ME"),
            h2TitlePostSelect: htmlDecode("rapport de peformance du syst&egrave;me"),
            welcome: "BIENVENUE",
            para1: htmlDecode("Terumo BCT promet la qualit&eacute; et dans cet objectif souhaite recevoir vos commentaires, suggestions et questions. La liste des rapports de performance ci-dessous nous aidera &agrave; mieux comprendre et &agrave; r&eacute;soudre rapidement les probl&egrave;mes que vous rencontrez avec nos kits de collecte &agrave; usage unique."),
            para2: htmlDecode("Dans ce but, veuillez suivre ces quelques &eacute;tapes simples:"),
            list: [htmlDecode("Localisez la zone du probl&egrave;me sur le sch&eacute;ma interactif."), htmlDecode("Remplissez le formulaire de renseignement sur l'incident.")],
            para3: htmlDecode("D&egrave;s que vous nous enverrez le formulaire en ligne, un membre de notre &eacute;quipe d'assistance client l'examinera pour y r&eacute;pondre."),
            beginButton: "Commencer",
            QorC: htmlDecode("Des questions?"),
            tollFree: "Appel gratuit",
            phoneNumber: "1.877.722.8411",
            aRR: htmlDecode("Tous droits r&eacute;serv&eacute;s"),
            quickOverview: htmlDecode("Aper&ccedil;u pratique"),
            startOver: "Recommencer",
            rfad: "Examiner Forme et Diagramme",
            backToDiagram: "Retour au Diagramme",
            reviewAndSubmit: "Examiner et Soumettre",
            backToEdit: htmlDecode("Retour &agrave; l'&eacute;dition"),
            print: "Imprimer pour vos dossiers",
            submit: "Soumettre",
            toolList: ["Couleur", "Outil", "Zoom", "Afficher Contexte", "Supprimer", "Terminer"],
            toolTipList: {
                color: "Changer de Couleur",
                tool: "Changement Outil",
                zoom: htmlDecode("Zoom Avant/Arri&egrave;re"),
                background: "Contexte Basculer",
                clear: "Supprimer",
                save: "Terminer"
            },
            products: [{
                    value: "",
                    label: htmlDecode("S&eacute;lectionner le produit...")
    }, {
                    value: null,
                    label: "------------------------------------------------------------------------------------------"
    }, {
                    value: null,
                    label: htmlDecode("Centres de collecte de sang")
    }, {
                    value: null,
                    label: "------------------------------------------------------------------------------------------"
    }, {
                    value: "Trima-PLT-RBC-PL",
                    label: htmlDecode("Kit de collecte Plaquettes, Plasma, H&eacute;maties Trima Accel&reg;"),
                    form: "trima"
    }, {
                    value: "Trima-DRBC",
                    label: htmlDecode("Kit de collecte H&eacute;maties, Plasma Trima Accel&reg;"),
                    form: "trima"
    }
                /*, {
                                value: "Teruflex",
                                label: htmlDecode("Syst&egrave;me de poche de sang Teruflex&reg;"),
                                form: "teruflex"
                    }, {
                                value: "Imuflex-WB-SP",
                                label: htmlDecode("Poches de sang - SP Imuflex&reg;"),
                                form: "imuflex"
                    }, {
                                value: "Imuflex-WB-RP",
                                label: htmlDecode("Poches de sang - SP Imuflex&reg;"),
                                form: "imuflex"
                    }
                , {
                    value: null,
                    label: "------------------------------------------------------------------------------------------"
    }, {
                    value: null,
                    label: htmlDecode("Traitement de cellules")
    }, {
                    value: null,
                    label: "------------------------------------------------------------------------------------------"
    }, {
                    value: "COBE-2991",
                    label: htmlDecode("Laveur de cellules COBE&reg; 2991 "),
                    form: "cobe2991"
    }, {
                    value: "Elutra",
                    label: htmlDecode("Syst&egrave;me de s&eacute;paration cellulaire Elutra&reg;"),
                    form: "elutra"
    }*/, {
                    value: null,
                    label: "------------------------------------------------------------------------------------------"
    }, {
                    value: null,
                    label: htmlDecode("Aph&eacute;r&egrave;se et collectes de cellules")
    }, {
                    value: null,
                    label: "------------------------------------------------------------------------------------------"
    },/* {
     value: "Cobe-Spectra-Collection",
     label: htmlDecode("Kit de collecte de leucocytes COBE&reg; Spectra"),
     form: "cobespectra"
 }, {
     value: "Cobe-Spectra-TPE",
     label: htmlDecode("Kit d'&eacute;change EH/EP COBE&reg; Spectra"),
     form: "cobespectra"
 }, {
     value: "Cobe-Spectra-AutoPBSC",
     label: htmlDecode("Kit AutoPBSC COBE &reg; Spectra"),
     form: "cobespectra"
 },*/ {
                    value: "OptiaExchange",
                    label: htmlDecode("Kit d'&eacute;change du syst&egrave;me d'aph&eacute;r&egrave;se Spectra Optia&reg;"),
                    form: "optiaexchange"
    }, {
                    value: "OptiaCollection",
                    label: htmlDecode("Kit de collecte du syst&egrave;me d'aph&eacute;r&egrave;se Spectra Optia&reg;"),
                    form: "optiacollection"
}]
        }
    };


    function htmlDecode(input) {
        var e = document.createElement("div");
        e.innerHTML = input;
        return e.childNodes[0].nodeValue;
    }

    $rootScope.currentLanguage = "en";

    $rootScope.selectHide = false;
    $rootScope.drawShow = false;
    $rootScope.showSubmit = false;

    $rootScope.finish = function () {
        $rootScope.previewImg = document.getElementById("canvas").toDataURL("image/png");
        $rootScope.formShow = false;
        var timer = $timeout(function () {
            $rootScope.formShow = true;
            $rootScope.showSubmit = true;
            $rootScope.previewShow = true;
            $rootScope.$apply();
            $timeout.cancel(timer);
        }, 1500);
    };

    $rootScope.submit = function () {
        document.getElementById("userForm").submit();
    };

    $rootScope.done = function () {
        $rootScope.drawShow = false;
        var timer = $timeout(function () {
            $rootScope.formShow = true;
            $rootScope.$apply();
            $timeout.cancel(timer);
        }, 1500);

    };

    $rootScope.backToDrawing = function () {
        $rootScope.formShow = false;
        $rootScope.previewShow = false;
        var timer = $timeout(function () {
            $rootScope.drawShow = true;
            $rootScope.showSubmit = false;
            $rootScope.$apply();
            $timeout.cancel(timer);
        }, 1500);
    };

    $rootScope.backToEdit = function () {
        $rootScope.formShow = false;
        var timer = $timeout(function () {
            $rootScope.previewShow = false;
            $rootScope.showSubmit = false;
            $rootScope.formShow = true;
            $rootScope.$apply();
            $timeout.cancel(timer);
        }, 1500);

    };

    $rootScope.print = function () {
        window.print();
    };

    $rootScope.$watch("currentLanguage", function (n) {
        if (n) {
            $rootScope.selectedProd = $rootScope.translationObj[n].products[0];
        }
    });

    $rootScope.goToProductPage = function () {
        if ($rootScope.selectedProd.value) {

            $rootScope.formName = "forms/" + $rootScope.currentLanguage + "/" + $rootScope.selectedProd.form + ".html";
            $rootScope.selectHide = true;

            var timer = $timeout(function () {
                $.each($(".datepicker"), function (i, v) {
                    $(v).datepicker();
                });

                $rootScope.translationObj[$rootScope.currentLanguage].h1Title = $rootScope.selectedProd.label;
                $rootScope.translationObj[$rootScope.currentLanguage].h2Title = $rootScope.translationObj[$rootScope.currentLanguage].h2TitlePostSelect;
                $rootScope.drawShow = true;
                $timeout.cancel(timer);
            }, 1500);

        }
    };
});